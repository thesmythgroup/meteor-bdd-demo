var reporter = require('cucumber-html-reporter');

var options = {
    theme: 'hierarchy',
    jsonFile: '.reports/acceptance-test-results.json',
    output: '.reports/acceptance-test-report.html',
    reportSuiteAsScenarios: true,
    launchReport: false,
    metadata: {
        "App Version":"0.0.0",
        "Test Environment": "LOCAL"
    }
};

reporter.generate(options);