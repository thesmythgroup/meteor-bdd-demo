Feature: I can login/logout

As a user of the system,
I want to be able to login and logout.
So I can use the application, and the application knows who I am.

Scenario: I can login
    Given I am not logged in
    And I am on the "Login" page
    When I login as "mmanager@example.com" with password "test123"
    Then I am logged in

Scenario: I can logout
    Given I am a "Requestor"
    When I click logout
    Then I am logged out
