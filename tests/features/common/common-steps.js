var url = require('url');
var testUsers = require('./test-users');
var appRoutes = require('./routes');

module.exports = function () {
    'use strict';

    this.Given(/^I am a "([^"]*)"$/, function (roleName) {
        // Find a user that has the proper access
        // (could also create a user matching the parameter)
        var userWithRole = testUsers[roleName];
        if ('undefined' === typeof userWithRole) {
            throw new Error(`No sample user is defined for type "${roleName}"`);
        }

        // generate test data will reset the DB, so we need to logout first or we will get a DDP
        // connection error
        server.call('logout');

        // see server/imports/test-data/generate-data.app-tests.ts for generated test data
        server.call('generateTestData');

        server.call('login', {user: {email: userWithRole.email}, password: userWithRole.password});
    });

    this.Given(/^I am on the "([^"]*)" page$/, function (pageName) {
        var path = appRoutes[pageName];

        if ('undefined' === typeof path) {
            throw new Error(`No routes defined for page "${pageName}"`);
        }

        client.url(url.resolve(process.env.ROOT_URL, path));
    });

    this.Given(/^I am not logged in$/, function () {
        server.call('logout');
    });
}

