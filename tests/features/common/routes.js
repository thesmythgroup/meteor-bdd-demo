module.exports = {
    'Login': '/login',
    'Create Request': '/requests/create',
    'Requests': '/requests/list',
    'View Request': '/requests/:_id'
};
