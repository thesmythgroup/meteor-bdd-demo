/**
 * This file exists because of the problem of sharing code between the JS step definitions
 * and the TS application.
 * Please keep /both/models/purchase-request-status.ts in sync with any changes
 */
module.exports = {
    'Draft': 0,
    'Submitted': 1,
    'Approved': 2,
    'Cancelled': 3
}
