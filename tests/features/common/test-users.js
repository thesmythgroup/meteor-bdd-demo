module.exports = {
    'Manager': {
        username: 'mmanager',
        email: 'mmanager@example.com',
        password: 'test123',
        name: 'Martin Manager'
    },
    'Requestor': {
        username: 'rrequestor',
        email: 'rrequestor@example.com',
        password: 'password',
        name: 'Ryan Requestor'
    }
};
