Feature: Approve a Purchase Request

As a manager in the system,
I want to approve purchase requests.
I do not want requestors to approve purchase requests.

Scenario: Manager can approve a purchase request
    Given I am a "Manager"
    When I approve a purchase request with the status "Submitted"
    Then the request has the status of "Approved"

Scenario: Requestor cannot approve a purchase request
    Given I am a "Requestor"
    When I approve a purchase request with the status "Submitted"
    Then the request has the status of "Submitted"
