Feature: Create a Purchase Request

As a requestor in the system,
I want to create and edit purchases requests.

Background:
    Given I am a "Requestor"
    And I am on the "Create Request" page

Scenario: Requestor can create valid purchase request
    When I enter the description "Office paper"
    And I enter the line items
    | description              | unitcost | quantity | cost  |
    | Box US Letter 4000 count | 24.99    | 1        | 24.99 |
    Then I can save the request
    And the request has the status of "Submitted"

