var url = require('url');
var expect = require('chai').expect;

var routes = require('../common/routes');
var PurchaseRequestStatus = require('../common/purchase-request-status');
var requests = require('./requests');


module.exports = function () {
    'use strict';

    var currentScenarioPRId = null;

    this.When(/^I approve a purchase request with the status "([^"]*)"$/, function (status) {
        // find a submitted purchase request
        var purchaseRequest = requests.findOne({status: PurchaseRequestStatus[status]});

        // save current scenario PR id
        currentScenarioPRId = purchaseRequest._id;

        // attempt to approve the request
        try {
            // call the approvePurchaseRequest method within the app context
            server.execute((id) => {
                return Meteor.call('approvePurchaseRequest', id);
            }, purchaseRequest._id);
        } catch(err) {
            // A failure response is not necessarily a failed step
            // success/fail of the scenario will be described in the then/assertion
        }
    });

    this.Then(/^the request has the status of "([^"]*)"$/, function (status) {
        var purchaseRequest = requests.findOne({_id: currentScenarioPRId});

        expect(purchaseRequest.status).to.equal(PurchaseRequestStatus[status]);
    });

}