module.exports = {
    findOne: function(search) {
        return server.execute((search) => {
            var { PurchaseRequests } = require('/both/collections/purchase-request.collection');

            return PurchaseRequests.findOne(search);
        }, search);
    }
}
