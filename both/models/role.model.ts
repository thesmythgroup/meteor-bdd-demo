import { Collection } from './_collection.model';

/**
 * Defines the `Role` entity.
 *
 * @export
 * @interface Role
 * @extends {Collection}
 */
export interface Role extends Collection {
    name: string;
    description?: string;
}
