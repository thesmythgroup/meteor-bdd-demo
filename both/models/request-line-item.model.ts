export interface RequestLineItem {
    description: string;
    unitCost: number;
    quantity: number;
    cost: number;
}
