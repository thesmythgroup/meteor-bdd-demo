import { Collection } from './_collection.model';

/**
 * Defines link between a `User` and `Role`.
 *
 * @export
 * @interface UserRole
 * @extends {Collection}
 */
export interface UserRole extends Collection {
    /**
     * Foreign Key
     * Links to `_id` on the Users collection.
     *
     * @type {string}
     * @memberof UserRole
     */
    userId: string;
    /**
     * Foreign Key
     * Links to `_id` on the Roles collection.
     *
     * @type {string}
     * @memberof UserRole
     */
    roleId: string;
}
