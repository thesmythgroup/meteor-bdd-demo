import { Collection } from './_collection.model';
import { RequestLineItem } from './request-line-item.model';
import { PurchaseRequestStatus } from './purchase-request-status';

export interface PurchaseRequest extends Collection {
    description: string;
    requestDate: Date;
    items: RequestLineItem[];
    total: number;
    status: PurchaseRequestStatus
}
