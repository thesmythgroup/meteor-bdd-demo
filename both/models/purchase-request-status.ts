/**
 * Due to the need to share code between the JS step definitions
 * and the TS application,
 * Please keep /tests/common/purchase-request-status.js in sync with any changes
 */
export enum PurchaseRequestStatus {
    Draft,
    Submitted,
    Approved,
    Cancelled
}
