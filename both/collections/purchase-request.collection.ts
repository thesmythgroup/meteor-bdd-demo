import { MongoObservable } from "meteor-rxjs";
import { PurchaseRequest } from "../models/purchase-request.model";

export const PurchaseRequests = new MongoObservable.Collection<PurchaseRequest>("purchaseRequests");
