import { MongoObservable } from 'meteor-rxjs';
import { UserRole } from '../models/user-role.model';

export const UserRoles = new MongoObservable.Collection<UserRole>('userRoles');
