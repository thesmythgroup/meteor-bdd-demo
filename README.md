# Meteor BDD Demo

This Demo application is based on the angular2-meteor-base project: https://github.com/Urigo/angular2-meteor-base

## NPM Scripts

The following scripts are defined `package.json`:

- `$ npm run start` - Run the Meteor application.
- `$ npm run start:prod` - Run the Meteor application in production mode.
- `$ npm run start:acceptance-test` - Run the Meteor application in e2e test (acceptance-test) mode.
- `$ npm run build` - Creates a Meteor build version under `./build/` directory.
- `$ npm run clear` - Resets Meteor's cache and clears the MongoDB collections.
- `$ npm run meteor:update` - Updates Meteor's version and it's dependencies.
- `$ npm run test` - Executes Meteor in test mode with Mocha.
- `$ npm run test:ci` - Executes Meteor in test mode with Mocha for CI (run once).
- `$ npm run chimp` - Run all of the acceptance tests using Chimp.
- `$ npm run chimpci` - Start the app in e2e test (acceptance-test) mode and run the acceptance tests (for CI).
- `$ npm run test-report:cucumber` - Generate HTML acceptance test report using Cucumber generator.
- `$ npm run test-report:pickles` - Generate HTML acceptance test report using Pickles generator.

## Demo Contents

This demo app contains some basic test examples for BDD.

Including:

- Tests impersonating various types of users
- Fixtures
- HTML report generation

### Folder Structure

The folder structure is a mix between [Angular 2 recommendation](https://johnpapa.net/angular-2-styles/) and [Meteor 1.3 recommendation](https://guide.meteor.com/structure.html).

### Client

The `client` folder contains single TypeScript (`.ts`) file which is the main file (`/client/app.component.ts`), and bootstrap's the Angular 2 application.

The main component uses HTML template and SASS file.

The `index.html` file is the main HTML which loads the application by using the main component selector (`<app>`).

All the other client files are under `client/imports` and organized by the context of the components (in our example, the context is `demo`).


### Server

The `server` folder contain single TypeScript (`.ts`) file which is the main file (`/server/main.ts`), and creates the main server instance, and the starts it.

All other server files should be located under `/server/imports`.

### Testing

The testing environment in this demo includes the [Meteor recommendation](https://guide.meteor.com/testing.html), useing Mocha as testing framework along with Chai for assertion.

In addition we have added chimp with cucumberjs for running acceptance tests.
