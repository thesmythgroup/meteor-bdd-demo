import './imports/publications/purchase-requests';
import { loadRoles } from './imports/fixtures/roles';

import { Main } from "./imports/server-main/main";

const mainInstance = new Main();
mainInstance.start();

Meteor.startup(() => {
    loadRoles();
});
