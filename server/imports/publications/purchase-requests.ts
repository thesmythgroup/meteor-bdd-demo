import { Meteor } from 'meteor/meteor';
import { PurchaseRequests } from '../../../both/collections/purchase-request.collection';
import { PurchaseRequestStatus } from '../../../both/models/purchase-request-status';
import { hasRole, ROLES } from '../lib/authorization';

Meteor.publish('purchaseRequests', function() {
    if (!hasRole.call(this, ROLES.Requestor)) {
        return;
    }

    return PurchaseRequests.find({});
});

Meteor.methods({
    approvePurchaseRequest(id) {
        var request = PurchaseRequests.findOne({_id: id});

        if (!request) {
            throw new Meteor.Error(404, "Not found");
        }

        // Only submitted requests can be approved
        if (request.status !== PurchaseRequestStatus.Submitted) {
            throw new Meteor.Error(400, "Invalid purchase request update");
        }

        if (!hasRole.call(this, ROLES.Manager)) {
            throw new Meteor.Error(403, "Forbidden");
        }

        PurchaseRequests.update({_id: id}, {$set:{status: PurchaseRequestStatus.Approved}});
    }
});

PurchaseRequests.allow({
    insert: function(userId, doc) {
        this.userId = userId;

        // Requestors create requests, but they cannot approve them
        return hasRole.call(this, ROLES.Requestor) &&
            (doc.status === PurchaseRequestStatus.Draft || doc.status === PurchaseRequestStatus.Submitted);
    },
    update: function(userId, doc) {
        this.userId = userId;

        // Manager can make any status update/change to the request
        var managerAuthorizedChange = hasRole.call(this, ROLES.Manager);

        // Requestor can only make updates/changes to draft/submitted requests
        var requestorAuthorizedChange = hasRole.call(this, ROLES.Requestor) &&
            (doc.status === PurchaseRequestStatus.Draft || doc.status === PurchaseRequestStatus.Submitted);

        return managerAuthorizedChange || requestorAuthorizedChange;
    }
});