import { Meteor } from 'meteor/meteor';
import { Roles } from '../../../both/collections/role.collection';

Meteor.publish('roles', function() {
    return Roles.find();
});
