import { Meteor } from 'meteor/meteor';
import { UserRoles } from '../../../both/collections/user-role.collection';
import { hasRole, ROLES } from '../lib/authorization';

Meteor.publish('user-role', function(userId: string) {
    if (!hasRole.call(this, ROLES.Admin)) {
        return;
    }

    return UserRoles.find({ userId: userId });
});

UserRoles.allow({
    insert: function(userId) {
        this.userId = userId;
        return hasRole.call(this, ROLES.Admin);
    },
    update: function(userId) {
        this.userId = userId;
        return hasRole.call(this, ROLES.Admin);
    }
});
