import { Meteor } from 'meteor/meteor';
import { Users } from '../../../both/collections/user.collection';
import { hasRole, ROLES } from '../lib/authorization';

Meteor.publish('users', function() {
    if (!hasRole.call(this, ROLES.Admin)) {
        return;
    }

    return Users.find({});
});

Users.allow({
    insert: function(userId) {
        this.userId = userId;
        return hasRole.call(this, ROLES.Admin);
    },
    update: function(userId) {
        this.userId = userId;
        return hasRole.call(this, ROLES.Admin);
    }
});
