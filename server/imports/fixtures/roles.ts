import { Roles } from '../../../both/collections/role.collection';
import { Role } from '../../../both/models/role.model';

export function loadRoles() {
  if (Roles.find().cursor.count() === 0) {

    const roles: Role[] = [{
      name: 'Admin',
      description: 'Administrator'
    }, {
      name: 'Manager',
      description: 'Manager'
    }, {
      name: 'Requestor',
      description: 'User authorized to enter requests'
    }];

    roles.forEach((role) => Roles.insert(role));
  }
}