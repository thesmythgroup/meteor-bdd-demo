import { Meteor } from 'meteor/meteor';
import { Roles } from '../../../both/collections/role.collection';
import { UserRoles } from '../../../both/collections/user-role.collection';

/**
 * Roles defined in the system.
 * Options are: `Admin`, `Manager`, `Requestor`.
 *
 * @export
 * @enum {number}
 */
export enum ROLES {
    /**
     * System Administrator
     */
    Admin,
    /**
     * Manager capable of approving requests
     */
    Manager,
    /**
     * User entering requests
     */
    Requestor
}

/**
 * Returns whether or not a user is logged in.
 *
 * @export
 * @returns {boolean}
 */
export function isLoggedIn(): boolean {
    if (this.userId === undefined) {
        return !!Meteor.userId();
    } else {
        return !!this.userId;
    }
}

/**
 * Returns whether or not the logged in user has a specific role.
 *
 * @export
 * @param {string} roleName
 * @returns {boolean}
 */
export function hasRole(role: ROLES): boolean {
    if (!isLoggedIn.call(this)) {
        return false;
    }

    let userRoles = UserRoles.findOne({ userId: this.userId || Meteor.userId() });

    if (!userRoles) {
        return false;
    }

    let roleRecord = Roles.findOne({ _id: userRoles.roleId });

    // Lesser role = greater priviledge. (ie: Admin = 0 vs Requestor = 2)
    return ROLES[roleRecord.name] <= role;
}
