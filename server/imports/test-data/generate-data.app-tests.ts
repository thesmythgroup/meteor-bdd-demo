import { Meteor } from 'meteor/meteor';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { promiseify } from '../utils/promiseify';
import { loadRoles } from '../fixtures/roles';
import { Roles } from '../../../both/collections/role.collection';
import { UserRoles } from '../../../both/collections/user-role.collection';
import { PurchaseRequest } from '../../../both/models/purchase-request.model';
import { PurchaseRequests } from '../../../both/collections/purchase-request.collection';
import { PurchaseRequestStatus } from '../../../both/models/purchase-request-status';

const testUsers = [
    {
        user: {
            username: 'mmanager',
            email: 'mmanager@example.com',
            password: 'test123',
            profile: {
                name: 'Martin Manager'
            }
        },
        role: 'Manager'
    },
    {
        user: {
            username: 'rrequestor',
            email: 'rrequestor@example.com',
            password: 'password',
            profile: {
                name: 'Ryan Requestor'
            }
        },
        role: 'Requestor'
    }
];

const testRequests: PurchaseRequest[] = [
    {
        description: 'Ice cream',
        requestDate: new Date('2017-06-01'),
        items: [
            {
                description: 'Vanilla, 1/2 gallon',
                unitCost: 3.99,
                quantity: 2,
                cost: 7.98
            },
            {
                description: 'Chocolate, 1/2 gallon',
                unitCost: 3.99,
                quantity: 1,
                cost: 3.99
            }
        ],
        total: 11.97,
        status: PurchaseRequestStatus.Submitted
    }
]

Meteor.methods({
    generateTestData() {
        resetDatabase();

        // reload fixtures
        loadRoles();

        testUsers.forEach((user) => {
            let userId = Accounts.createUser(user.user);
            let role = Roles.findOne({name: user.role});

            UserRoles.insert({userId: userId, roleId: role._id});
        });
        testRequests.forEach((request) => {
            PurchaseRequests.insert(request);
        });
    }
});
