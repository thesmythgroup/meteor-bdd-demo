import { Meteor } from 'meteor/meteor';
import { OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, Subscription, Subject } from "rxjs";
import { MeteorObservable } from "meteor-rxjs";
import { ActivatedRoute, Params } from '@angular/router';

/**
 * Base entity class for quick development of core CRUD statements on simple entities.
 *
 * @export
 * @class Entity
 * @implements {OnInit}
 * @template T
 */
export class Entity<T> implements OnInit, OnDestroy {

    public record: T;
    public typeName: string;
    public pk: string;
    public form: FormGroup;
    public saveText: string = 'Save';
    public saveClass: string = 'btn-primary';
    private isReadOnly: boolean;
    private recordSub: Subscription;
    private safeCopy: T;

    constructor(public defaultRecord: T, public route: ActivatedRoute, public collection: any) {
        this.record = defaultRecord;
    }

    ngOnInit() {
        if (this.route) {
            this.route.params.subscribe(params => {
                this.pk = params['_id'];
                this.read();
            });
        } else if (this.record['_id']) {
            this.pk = this.record['_id'];
            this.read();
        } else {
            // assume record was hydrated from component
            this.safeCopy = JSON.parse(JSON.stringify(this.record));
        }
    }

    ngOnDestroy() {
        if (this.recordSub) {
            this.recordSub.unsubscribe();
        }
    }

    /**
     * Subscribe to the publication of the record at hand and get this entity.
     * only called if pk is available.
     *
     *
     * @memberof Entity
     */
    public read(): void {
        // todo: return the observable for chaining
        if (this.pk && this.pk !== "0") {
           this.recordSub = MeteorObservable.subscribe(this.typeName).subscribe(() => {
                Object.assign(this.record, this.collection.findOne(this.pk));
                this.safeCopy = JSON.parse(JSON.stringify(this.record));
            });
        }
    }

    /**
     * Either insert or update an object based on pk.
     *
     * @memberof Entity
     */
    public save(): void {
        // todo: return the observable for chaining

        if (!this.validate()) return;

        // assume having a pk means the record exists
        if (this.pk && this.pk !== "0") {

            let obj: any = {};
            for (let prop in this.record) {
                if (prop !== '_id') {
                    obj[prop] = this.record[prop];
                }
            }

            this.collection.update({_id: this.pk}, { $set: obj} );

        } else {
            this.collection.insert(this.record).subscribe((id: string) => {
                this.record['_id'] = id;
                this.pk = id;
            });
        }

        this.toggleSave();
    }

    /**
     * Override in implementing class.
     *
     * @memberof Entity
     */
    public validate(): boolean {
        return this.form.valid;
    }

    /**
     * Flip the switch on read only.
     *
     * @memberof Entity
     */
    public toggleReadOnly(): void {
        if (this.isReadOnly) {
            this.isReadOnly = false;
            this.safeCopy = JSON.parse(JSON.stringify(this.record));
        } else {
            this.isReadOnly = true;
        }
    }

    public toggleSave(): void {
        this.saveText = 'Saved Successfuly!';
        this.saveClass = 'btn-success';

        setTimeout(() => {
            this.saveText = 'Save';
            this.saveClass = 'btn-primary';
        }, 1500);
    }

    /**
     * Reset the record to its original state.
     *
     * @memberof Entity
     */
    public reset(): void {
        // todo: Use FormBuilder or use native form.reset();
        this.record = JSON.parse(JSON.stringify(this.safeCopy));
        this.isReadOnly = true;
    }
}