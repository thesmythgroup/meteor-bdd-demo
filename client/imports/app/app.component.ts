import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { Meteor } from 'meteor/meteor';
import { InjectUser } from 'angular2-meteor-accounts-ui';
import template from "./app.component.html";
import style from "./app.component.scss";

@Component({
  selector: "app",
  template,
  styles: [ style ]
})

@InjectUser('user')
export class AppComponent {
  public user: Meteor.User;
  constructor(private router: Router) {}

  public logOff() {
    Accounts.logout(() => {
      this.router.navigate(['/login']);
    });
  }
}
