import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { PurchaseRequest } from "../../../../both/models/purchase-request.model";
import template from "./requests.component.html";
import style from "./requests.component.scss";

@Component({
  selector: "requests",
  template,
  styles: [ style ]
})
export class RequestsComponent implements OnInit {
  greeting: string;

  constructor() {
    this.greeting = "Requests Component!";
  }

  ngOnInit() {}
}
