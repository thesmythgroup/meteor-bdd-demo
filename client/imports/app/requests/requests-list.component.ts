import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meteor } from 'meteor/meteor';
import { Observable, Subscription, Subject } from "rxjs";
import { MeteorObservable } from "meteor-rxjs";
import { PurchaseRequest } from "../../../../both/models/purchase-request.model";
import { PurchaseRequests } from "../../../../both/collections/purchase-request.collection";
import style from "./requests-list.component.scss";
import template from "./requests-list.component.html";

@Component({
    selector: 'requests-list',
    template,
    styles: [ style ]
})
export class RequestsListComponent implements OnInit, OnDestroy {
    requests: Observable<PurchaseRequest[]>;
    requestsSub: Subscription;

    constructor() {}

    ngOnInit() {
        this.requests = PurchaseRequests.find({}).zone();
        this.requestsSub = MeteorObservable.subscribe('purchaseRequests').subscribe();
    }

    ngOnDestroy() {
        this.requestsSub.unsubscribe();
    }
}
