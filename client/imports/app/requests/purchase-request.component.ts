import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Meteor } from 'meteor/meteor';
import { Observable, Subscription, Subject } from "rxjs";
import { MeteorObservable } from "meteor-rxjs";
import style from "./purchase-request.component.scss";
import template from "./purchase-request.component.html";
import { Entity } from '../../../lib/entity.component';
import { PurchaseRequest } from "../../../../both/models/purchase-request.model";
import { RequestLineItem } from "../../../../both/models/request-line-item.model";
import { PurchaseRequests } from "../../../../both/collections/purchase-request.collection";
import { PurchaseRequestStatus } from "../../../../both/models/purchase-request-status";
import { User } from "../../../../both/models/user.model";
import { Users } from "../../../../both/collections/user.collection";


@Component({
    selector: 'purchase-request',
    template,
    styles: [ style ]
})
export class PurchaseRequestComponent extends Entity<PurchaseRequest> implements OnInit, OnDestroy {
    constructor(route: ActivatedRoute, private ngZone: NgZone, private formBuilder: FormBuilder) {
        super({
            description: '',
            requestDate: new Date(),
            items: [],
            total: 0,
            status: PurchaseRequestStatus.Draft,
        }, route, PurchaseRequests);
        this.typeName = 'purchaseRequests';
    }

    ngOnInit() {
        super.ngOnInit();

        this.form = this.formBuilder.group({
            description: ['', [Validators.required, Validators.minLength(2)]],
            requestDate: [new Date()],
            items: [[], [Validators.required, Validators.minLength(1)]],
        });

    }

    ngOnDestroy() {
    }

    addItem() {
        this.record.items.push({
            description: '',
            unitCost: 0,
            quantity: 0,
            cost: 0
        });
    }
}
