import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Meteor } from 'meteor/meteor';
import { InjectUser } from 'angular2-meteor-accounts-ui';
import style from "./login.component.scss";
import template from "./login.component.html";

@Component({
    selector: 'login-form',
    template,
    styles: [ style ]
})

@InjectUser('user')
export class LoginComponent implements OnInit {
    public user: Meteor.User;
    public mode: string = 'login'

    constructor(private router:Router) {}

    public ngOnInit(): void {
        let userId = Meteor.userId();
        if (userId) {
            this.navigateToDefault();
        }
    }

    public changeMode(mode: string): void {
        this.mode = mode;
    }

    public login(email: string, password: string): void {
        Meteor.loginWithPassword(email, password, (err) => {
            if (err) {
                alert(err);
            } else
                this.navigateToDefault();
        });
    }

    public facebook(): void {
        Meteor.loginWithFacebook(null, (err) => {
            if (err)
                alert(err);
            else
                this.navigateToDefault();
        });
    }

    public google(): void {
        Meteor.loginWithGoogle(null, (err) => {
            if (err)
                alert(err);
            else
                this.navigateToDefault();
        });
    }

    public signOut(): void {
        Meteor.logout((err) => {
            if (err) alert(err);
        });

        this.changeMode('login');
    }

    public forgotPassword(email: string): void {
        Accounts.forgotPassword({ email: email}, (err) => {
            if (err) alert(err);
        })
    }

    public register(email: string, password: string, name: string): void {
        let id;
        id = Accounts.createUser({
            email: email,
            password: password,
            profile: {
                name: name
            }
        });
    }

    public navigateToDefault() {
        this.router.navigate(['/requests']);
    }
}
