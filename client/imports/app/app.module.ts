import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from "./app.component";
import { LoginComponent } from './login/login.component';
import { RequestsComponent } from "./requests/requests.component";
import { RequestsListComponent } from "./requests/requests-list.component";
import { PurchaseRequestComponent } from "./requests/purchase-request.component";

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'requests',
    component: RequestsComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full'},
      { path: 'list', component: RequestsListComponent },
      { path: 'create', component: PurchaseRequestComponent },
      { path: ':_id', component: PurchaseRequestComponent }
    ]
  },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  // Components, Pipes, Directive
  declarations: [
    AppComponent,
    RequestsComponent,
    RequestsListComponent,
    PurchaseRequestComponent,
    LoginComponent
  ],
  // Entry Components
  entryComponents: [
    AppComponent
  ],
  // Providers
  providers: [],
  // Modules
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  // Main Component
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor() {

  }
}
